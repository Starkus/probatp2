8. Los estimadores de la muestra dada son:

	* Momentos: 3.642933
	* MV: 20.1
	* 2 Medianas: 1.06

Podemos ver que el menos afectado por outliers es el tercero

9. La probabilidad de que una muestra este contaminada es de 1-(1-0.005)^n o ~0.072 con n=15

Los estimadores simulados con outliers dieron los siguientes valores:

	* Momentos:
		- Sesgo:
			0.539572
		- Varianza:
			3.957455
		- ECM:
			0.004704
	* MV:
		- Sesgo:
			4.815155
		- Varianza:
			262.0571
		- ECM:
			0.05870622
	* 2 Medianas:
		- Sesgo:
			5.106293
		- Varianza:
			277.7186
		- ECM:
			0.05872835

Devuelta vemos la vulnerabilidad de el estimador de maxima verosimilitud frente a valores de muestra muy grandes. En caso de tener outliers el estimador mas robusto parece ser el de momentos.
