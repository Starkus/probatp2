generarMuestra = function(n, a, b) {
	len = b - a
	x = runif(n) * len
	return(x)
}

generarMuestraConErrores = function(n, a, b) {
	x = generarMuestra(n, a, b)
	for (i in 1:n) {
		if (runif(1) < 0.005) {
			x[i] = x[i] * 100
		}
	}
	return(x)
}

estimadorMom = function(x) {
	return(2 * mean(x))
}

estimadorMv = function(x) {
	return(max(x))
}

estimadorMed = function(x) {
	return(2 * median(x))
}

simulacion = function(n) {
	x = c()
	xMom = c()
	xMv = c()
	xMed = c()
	for (i in 1:n) {
		xi = generarMuestra(15, 0, 1)

		x = c(x, xi)
		xMom = c(xMom, estimadorMom(xi))
		xMv = c(xMv, estimadorMv(xi))
		xMed = c(xMed, estimadorMed(xi))
	}

	sesgoMom = mean(xMom) - 1
	sesgoMv = mean(xMv) - 1
	sesgoMed = mean(xMed) - 1

	print("Sesgo")
	print(sesgoMom)
	print(sesgoMv)
	print(sesgoMed)

	varMom = var(xMom)
	varMv = var(xMv)
	varMed = var(xMed)

	print ("Varianza")
	print(varMom)
	print(varMv)
	print(varMed)

	ecmMom = varMom + sesgoMom^2
	ecmMv = varMv + sesgoMv^2
	ecmMed = varMed + sesgoMed^2

	print("Error cuadratico medio")
	print(ecmMom)
	print(ecmMv)
	print(ecmMed)
}

simulacionMom = function(b, n) {
	x = c()
	for (i in 1:1000) {
		y = generarMuestra(n, 0, b)
		x = c(x, estimadorMom(y))
	}
	
	#hist(x, main="Momentos", breaks=15)
	plot(x, col='blue', ylim=c(0.75, 1.25), ysize=)
	
	sesgo = mean(x) - b
	return(sesgo^2 + var(x))
}

simulacionMv = function(b, n) {
	x = c()
	for (i in 1:1000) {
		y = generarMuestra(n, 0, b)
		x = c(x, estimadorMv(y))
	}
	
	#hist(x, main="Max verosimilitud")
	points(x, col='red')
	
	sesgo = mean(x) - b
	return(sesgo^2 + var(x))
}

simulacionMed = function(b, n) {
	x = c()
	for (i in 1:1000) {
		y = generarMuestra(n, 0, b)
		x = c(x, estimadorMed(y))
	}
	
	#hist(x, main="2 medianas", breaks=15)
	points(x, col='green')
	
	legend("topleft",
	       c("Momentos","Maxima verosimilitud", "2 medianas"),
	       fill=c("blue","red", "green"))
	
	sesgo = mean(x) - b
	return(sesgo^2 + var(x))
}

#simulacion(1000)
n = 240
simulacionMom(1, n)
simulacionMv(1, n)
simulacionMed(1, n)
